# ywc-cli

This is a tool to improve the efficiency of project development

If you are a novice, you can use this tool to build projects without obstacles

If you are a senior engineer, you can save a lot of time in creating projects through this tool

## command

` c/create ` 

ywc-cli c/create project